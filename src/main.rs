use anyhow::anyhow;
use industrial_io as iio;
use std::{process::Command, thread::sleep, time::Duration};

mod sensors;

pub type Error = Box<dyn std::error::Error>;

pub enum Update {
    Accel([f64; 3]),
    Angle(f64),
}

#[derive(Copy, Clone, Eq, PartialEq)]
enum Direction {
    XPlus,
    YPlus,
    ZPlus,
    XMinus,
    YMinus,
    ZMinus,
}

use Direction::*;

impl Direction {
    pub fn negate(self) -> Self {
        match self {
            XPlus => XMinus,
            YPlus => YMinus,
            ZPlus => ZMinus,
            XMinus => XPlus,
            YMinus => YPlus,
            ZMinus => ZPlus,
        }
    }

    pub fn rotation(self) -> Option<u32> {
        match self {
            YPlus => Some(0),
            XPlus => Some(90),
            YMinus => Some(180),
            XMinus => Some(270),
            _ => None,
        }
    }
}

pub fn set_rotation(rotation: u32) -> Result<(), Error> {
    Command::new("swaymsg")
        .arg("output")
        .arg("eDP-1")
        .arg("transform")
        .arg(format!("{}", rotation))
        .arg("scale")
        .arg("2")
        .spawn()?;
    Ok(())
}

const MIN_ACCELERATION: f64 = 3.0;
const MIN_ANGLE: f64 = 180.0;
const UPDATE_AFTER_SAMPLES: usize = 2;

fn main() -> Result<(), Error> {
    let mut ctx = iio::Context::create_local()?;
    ctx.set_timeout_ms(0)?; // disable timeout

    let mut accel_dev = None;
    let mut angle_dev = None;

    for device in ctx.devices() {
        let name = match device.name() {
            Some(i) => i,
            None => continue,
        };
        let attrs = device.attr_read_all()?;

        match (name.as_str(), attrs.get("location").map(String::as_str)) {
            ("cros-ec-accel", Some("lid")) => accel_dev = Some(device),
            ("cros-ec-lid-angle", _) => angle_dev = Some(device),
            _ => (),
        }
    }

    let accel_dev = accel_dev.ok_or(anyhow!("no chromebook lid accelerometer detected"))?;
    let angle_dev = angle_dev.ok_or(anyhow!("no chromebook lid angle sensor detected"))?;

    let mut accel = sensors::Accelerometer::new(accel_dev)?;
    let mut angle = sensors::Protractor::new(angle_dev)?;

    let mut direction = YPlus;
    let mut direction_age = 0usize;
    let mut tablet_mode = false;

    loop {
        sleep(Duration::from_millis(300));

        let lid_angle = angle.read()?;
        if 0.0 <= lid_angle && lid_angle < MIN_ANGLE {
            tablet_mode = false;
        } else if lid_angle <= 360.0 {
            tablet_mode = true;
        }

        let mut new_direction = None;

        let [x, y, z] = accel.read()?;
        if tablet_mode {
            if x * x + y * y + z * z > MIN_ACCELERATION * MIN_ACCELERATION {
                let (v, mut d) = [(x, XPlus), (y, YPlus), (z, ZPlus)]
                    .iter()
                    .copied()
                    .max_by(|(a, _), (b, _)| a.abs().partial_cmp(&b.abs()).unwrap())
                    .unwrap();

                if v < 0. {
                    d = d.negate();
                }

                new_direction = Some(d);
            }
        } else {
            new_direction = Some(YPlus);
        }

        if let Some(new_direction) = new_direction {
            if direction == new_direction {
                direction_age = direction_age.saturating_add(1);
            } else {
                direction = new_direction;
                direction_age = 0;
            }

            if direction_age == UPDATE_AFTER_SAMPLES {
                if let Some(rotation) = direction.rotation() {
                    set_rotation(rotation)?;
                }
            }
        }
    }
}
